//
//  DropablePin.swift
//  pixel-city
//
//  Created by Panupong Kukutapan on 11/14/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import UIKit
import MapKit

class DropablePin: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var identifier: String
    
    init(coordinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }
}
