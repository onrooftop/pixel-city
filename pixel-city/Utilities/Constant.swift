//
//  Constant.swift
//  pixel-city
//
//  Created by Panupong Kukutapan on 11/14/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import Foundation

let API_KEY = "9039ab58e63f2bee69e6e112d4db0032"


func flickrUrl(forApiKey key: String, widhAnnotation annotation: DropablePin, andNumberOfPhotos number: Int) -> String{
    
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(key)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}


